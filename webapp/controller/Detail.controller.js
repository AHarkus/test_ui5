sap.ui.define([
	"Test_UI5/Test_UI5/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("Test_UI5.Test_UI5.controller.Detail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf Test_UI5.Test_UI5.view.Detail
		 */
		onInit: function () {
			/*Set up routing*/
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {
			/*Get argument passed from Main page (productId)*/
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");

			/*Set up the oView var to be the view*/
			oView = this.getView();

			/* Bind the path to the view so that the correct product is display */
			oView.bindElement({
				path: "/Products(" + oArgs.productId + ")"
			});
		},

		onLiveChange: function (oEvent) {
			/* When any of the product details are changed ... */

			/* Hide back button */
			var oButtonBack = this.getView().byId("buttonBack");
			oButtonBack.setEnabled(false);
			/* Show Save button */
			var oButtonSave = this.getView().byId("buttonSave");
			oButtonSave.setEnabled(true);
			/* Show Cancel button */
			var oButtonCancel = this.getView().byId("buttonCancel");
			oButtonCancel.setEnabled(true);
		},

		onCancel: function (oEvent) {
			/* When the cancel button is pressed ... */

			/* Undo any changes */
			this.getView().getModel().resetChanges();
			/* Hide Save button */
			var oButtonSave = this.getView().byId("buttonSave");
			oButtonSave.setEnabled(false);
			/* Show Back button */
			var oButtonBack = this.getView().byId("buttonBack");
			oButtonBack.setEnabled(true);
			/* Hide Cancel button */
			var oButtonCancel = this.getView().byId("buttonCancel");
			oButtonCancel.setEnabled(false);
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Test_UI5.Test_UI5.view.Detail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Test_UI5.Test_UI5.view.Detail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Test_UI5.Test_UI5.view.Detail
		 */
		//	onExit: function() {
		//
		//	}

	});

});
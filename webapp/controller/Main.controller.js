sap.ui.define([
	"Test_UI5/Test_UI5/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("Test_UI5.Test_UI5.controller.Main", {
		onInit: function () {

		},
		/* Nav to Detail page */
		onSelected: function (oEvent) {
			var oItem, oCtx;
			oItem = oEvent.getSource();
			oCtx = oItem.getBindingContext();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("detail", {
				productId: oCtx.getProperty("productId")
			});
		}
	});
});
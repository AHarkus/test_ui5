	/*global QUnit*/

	sap.ui.define([
		"sap/ui/test/opaQunit",
		"./pages/Main",
		"./pages/Detail"
	], function (opaTest) {
		"use strict";

		QUnit.module("Navigation Journey");

		opaTest("Should see the initial page of the app", function (Given, When, Then) {
			// Arrangements
			Given.iStartMyApp();

			// Assertions
			Then.onTheMainPage.iShouldSeeTheApp();

		});
		
		opaTest("Should see the post details page for the product", function (Given, When, Then) {
			//Actions
			When.onTheMainPage.iPressOnTheItemWithTheId(9);

			// Assertions
			Then.onTheDetailPage.theTitleShouldDisplayTheId("Product ID : 9");
			
			//Cleanup
			Then.iTeardownMyApp();
		});

	});
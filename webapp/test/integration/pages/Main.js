sap.ui.define([
	"sap/ui/test/Opa5",
	'sap/ui/test/matchers/BindingPath',
	'sap/ui/test/actions/Press'
], function (Opa5, BindingPath, Press) {
	"use strict";
	var sViewName = "Main";
	Opa5.createPageObjects({
		onTheMainPage: {

			actions: {
				iPressOnTheItemWithTheId: function (sId) {
					return this.waitFor({
						controlType: "sap.m.ColumnListItem",
						viewName: sViewName,
						matchers: new BindingPath({
							path: "/Products(" + sId + ")"
						}),
						actions: new Press(),
						success: function () {
							Opa5.assert.ok(true, "Navigated from Main View.");
						},
						errorMessage: "No list item with the ID " + sId + " was found."
					});
				}
			},

			assertions: {
				iShouldSeeTheApp: function () {
					return this.waitFor({
						viewName: sViewName,
						success: function () {
							Opa5.assert.ok(true, "The Main view is displayed");
						},
						errorMessage: "Did not find the Main view"
					});
				}
			}
		}
	});

});
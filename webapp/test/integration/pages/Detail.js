sap.ui.define([
	'sap/ui/test/Opa5',
	'sap/ui/test/matchers/Properties'
], function (Opa5, Properties) {
		"use strict";
		var sViewName = "Detail";
		Opa5.createPageObjects({
			onTheDetailPage: {
				actions: {
				},
				assertions: {
					theTitleShouldDisplayTheId: function (sName) {
						return this.waitFor({
							success: function () {
								return this.waitFor({
									id: "objectHeader",
									viewName: sViewName,
									matchers: new Properties({
										text: sName
									}),
									success: function (oPage) {
										Opa5.assert.ok(true, "Product successfully displayed");
									},
									errorMessage: "The Post " + sName + " is not shown"
								});
							}
						});
					}
				}
			}
		});
	});